create table Q9 as select category_name,sum(item_price) as total_price,
item.category_id from item
inner join item_category
on item.category_id = item_category.category_id
group by category_id order by total_price desc;

select category_name,total_price from Q9;
