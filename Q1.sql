CREATE DATABASE Q1 DEFAULT CHARACTER SET utf8;

CREATE TABLE itme_category(
  category_id int not null auto_increment primary key,
  category_name varchar(256) not null
);
